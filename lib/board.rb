class Board
  attr_accessor :grid

  def self.default_grid
    default_grid = Array.new(10) { Array.new(10) }
  end

  def initialize(grid = self.class.default_grid)
    @grid = grid
  end

  def count
    @grid.flatten.reject { |el| el == nil }.length
  end

  def [](pos)
    x, y = pos
    grid[x][y]
  end

  def []=(pos, val)
    x, y = pos
    grid[x][y] = val
  end

  def empty?(pos = nil)
    pos ? self[pos].nil? : @grid.flatten.all? {|el| el.nil?}
  end

  def full?
    grid.flatten.none?(&:nil?)
  end

  def place_random_ship
    raise "Error, I'm sorry Dave, I cant let you do that" if full?
    pos = [rand(grid.length), rand(grid.length)]
    pos = [rand(grid.length), rand(grid.length)] until empty?(pos)
    self[pos] = :s
  end

  def won?
    grid.flatten.none? { |el| el == :s }
  end
end
